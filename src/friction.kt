fun main() {
    val x = Fraction(12.0, 8.0)
    println(x)
    val y = Fraction(16.0, 9.0)
    println(y)
    println(x.add(y))
    println(x.multiply(y))
    println(x.minus(y))
    println(x.divide(y))


}

class Fraction(val Nominator: Double, val Denominator: Double) {
    override fun toString(): String {
        return "$Nominator $Denominator"
    }

    override fun equals(other: Any?): Boolean {
        if (other is Fraction) {
            return (Nominator * other.Denominator == other.Nominator * Denominator)
        }
        return false
    }

    fun add(other: Fraction): Fraction {
        val newDenominator = Denominator * other.Denominator
        val newNomirator1 = newDenominator / Denominator * Nominator
        val newNomirator2 = newDenominator / other.Denominator * other.Nominator
        return Fraction(newNomirator1 + newNomirator2, newDenominator)

    }

    fun multiply(other: Fraction): Fraction {
        val Mnominator = Nominator * other.Nominator
        val Mdomirator = Denominator * other.Denominator
        return Fraction(Mnominator, Mdomirator)
    }

    fun minus(other: Fraction): Fraction {
        return add(Fraction(-1 * other.Nominator, other.Denominator))
    }

    fun divide(other: Fraction): Fraction {
        return multiply(Fraction(other.Denominator, other.Nominator))
    }
}