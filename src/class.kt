fun main() {

    val point1 = Point(69.0,45.0)

    println(point1)

    val point2 = Point(22.0,23.0)

    println(point2)

    val point3 = point1.moveInOpositeDirection()

    println(point3)

    val point4 = point2.moveInOpositeDirection()

    println(point4)

}

class Point(private var x: Double, private val y: Double) {


    override fun toString(): String {
        return "(${x},${y})"
    }
    override fun equals(other: Any?): Boolean {
        if (other is Point) {
            val areXsEqual = x == other.x
            val areYsEqual = y == other.y
            return areXsEqual && areYsEqual
        }


        return false
    }

    fun moveInOpositeDirection(): Point {
        return Point(-1 * x,-1 * y)
        
    }
}